package dev.art.booksearch.presenter

import dev.art.booksearch.data.repository.BookRepository
import dev.art.booksearch.ui.base.SchedulerTestProvider
import dev.art.booksearch.ui.list.ListMvpView
import dev.art.booksearch.ui.list.ListPresenter
import dev.art.booksearch.ui.list.MyDataSource
import io.reactivex.subjects.PublishSubject
import org.junit.Before
import org.junit.Test
import org.mockito.Mockito.*

class ListPresenterTest {
    lateinit var presenter: ListPresenter
    lateinit var repository: BookRepository
    lateinit var view: ListMvpView
    private val testQuery = "Face In The Mirror"

    @Before
    fun init() {
        view = mock(ListMvpView::class.java)
        repository = mock(BookRepository::class.java)

        presenter = ListPresenter(repository, SchedulerTestProvider())
        presenter.attachView(view)
    }

    @Test
    fun checkShowProgressLoadingData() {
        val dataSource = mock(MyDataSource::class.java)
        presenter.setDataSource(dataSource)
        val publishSubject = PublishSubject.create<MyDataSource.StateLoading>()
        `when`(dataSource.getLoading()).thenReturn(publishSubject)

        presenter.initLoadingListener()
        publishSubject.onNext(MyDataSource.StateLoading.LOADING)

        verify(view, times(1)).showProgress()
    }

    @Test
    fun checkHideProgressLoadingWhenSuccessLoadData() {
        val dataSource = mock(MyDataSource::class.java)
        presenter.setDataSource(dataSource)
        val publishSubject = PublishSubject.create<MyDataSource.StateLoading>()
        `when`(dataSource.getLoading()).thenReturn(publishSubject)

        presenter.initLoadingListener()
        publishSubject.onNext(MyDataSource.StateLoading.SUCCESS)

        verify(view, times(1)).hideProgress()
    }

    @Test
    fun checkHideProgressLoadingWhenErrorLoadData() {
        val dataSource = mock(MyDataSource::class.java)
        presenter.setDataSource(dataSource)
        val publishSubject = PublishSubject.create<MyDataSource.StateLoading>()
        `when`(dataSource.getLoading()).thenReturn(publishSubject)

        presenter.initLoadingListener()
        publishSubject.onNext(MyDataSource.StateLoading.ERROR)

        verify(view, times(1)).hideProgress()
    }
}