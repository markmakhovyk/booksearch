package dev.art.booksearch.presenter

import dev.art.booksearch.data.model.Response
import dev.art.booksearch.data.repository.BookRepository
import dev.art.booksearch.ui.base.SchedulerTestProvider
import dev.art.booksearch.ui.search.SearchMvpView
import dev.art.booksearch.ui.search.SearchPresenter
import io.reactivex.Single
import org.junit.Before
import org.junit.Test
import org.mockito.ArgumentMatchers
import org.mockito.Mockito.*

class SearchPresenterTest {
    lateinit var presenter: SearchPresenter
    lateinit var repository: BookRepository
    lateinit var view: SearchMvpView
    private val testQuery = "Face In The Mirror"

    @Before
    fun init() {
        view = mock(SearchMvpView::class.java)
        repository = mock(BookRepository::class.java)

        presenter = SearchPresenter(repository, SchedulerTestProvider())
        presenter.attachView(view)

    }

    @Test
    fun checkShowProgressLoadingData() {
        val response = mock(Response::class.java)
        `when`(repository.search(ArgumentMatchers.any())).thenReturn(Single.just(response))
        `when`(response.numFound).thenReturn(0)
        presenter.search(testQuery)

        verify(view, times(1)).showProgress()
    }

    @Test
    fun checkHideProgressWhenSuccessLoading() {
        val response = mock(Response::class.java)
        `when`(repository.search(ArgumentMatchers.any())).thenReturn(Single.just(response))
        `when`(response.numFound).thenReturn(0)
        presenter.search(testQuery)

        verify(view, times(1)).hideProgress()
    }

    @Test
    fun checkHideProgressWhenErrorLoading() {
        `when`(repository.search(testQuery)).thenReturn(Single.error(mock(Throwable::class.java)))
        presenter.search(testQuery)

        verify(view, times(1)).hideProgress()
    }

    @Test
    fun checkShowMessageAboutEmptyData() {
        val response = mock(Response::class.java)
        `when`(repository.search(testQuery)).thenReturn(Single.just(response))
        `when`(response.numFound).thenReturn(0)
        presenter.search(testQuery)

        verify(view, times(1)).showEmpty(testQuery)
    }

    @Test
    fun checkShowMessageAboutSuccessData() {
        val response = mock(Response::class.java)
        `when`(repository.search(testQuery)).thenReturn(Single.just(response))
        `when`(response.numFound).thenReturn(2)
        presenter.search(testQuery)

        verify(view, times(1)).showSuccessResult(2, testQuery)
    }

    @Test
    fun checkShowMessageAboutError() {
        `when`(repository.search(testQuery)).thenReturn(Single.error(mock(Throwable::class.java)))
        presenter.search(testQuery)

        verify(view, times(1)).showError()
    }
}