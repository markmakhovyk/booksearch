package dev.art.booksearch

import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.action.ViewActions.*
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.ViewMatchers.*
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.rule.ActivityTestRule
import dev.art.booksearch.ui.MainActivity
import org.hamcrest.CoreMatchers.not
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith

/**
 * Instrumented test, which will execute on an Android device.
 *
 * See [testing documentation](http://d.android.com/tools/testing).
 */
@RunWith(AndroidJUnit4::class)
class SearchScreenTest {

    @Rule
    @JvmField
    var activityActivityTestRule = ActivityTestRule(MainActivity::class.java)

    @Test
    fun checkEnableButtonSearch() {
        onView(withId(R.id.etQuery)).perform(typeText("Test"))
        onView(withId(R.id.btnSearch)).check(matches(isEnabled()))
    }

    @Test
    fun checkDisableButtonSearch() {
        onView(withId(R.id.etQuery)).perform(clearText())
        onView(withId(R.id.btnSearch)).check(matches(not(isEnabled())))
    }

    @Test
    fun checkShowProgressLoading() {
        onView(withId(R.id.etQuery)).perform(typeText("Test"))
        onView(withId(R.id.btnSearch)).perform(click())
        onView(withId(R.id.progressBar)).check(matches(isDisplayed()))
    }

    @Test
    fun checkNotShowButtonAndInputFieldWhenLoading() {
        onView(withId(R.id.etQuery)).perform(typeText("Test"))
        onView(withId(R.id.btnSearch)).perform(click())
        onView(withId(R.id.etQuery)).check(matches(not(isDisplayed())))
        onView(withId(R.id.btnSearch)).check(matches(not(isDisplayed())))
    }
}
