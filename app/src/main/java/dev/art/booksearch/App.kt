package dev.art.booksearch


import android.app.Application
import dev.art.booksearch.di.AppComponent
import dev.art.booksearch.di.AppModule
import dev.art.booksearch.di.DaggerAppComponent

class App : Application() {

    val component: AppComponent?
        get() = appComponent

    override fun onCreate() {
        super.onCreate()
        appComponent = DaggerAppComponent.builder()
            .appModule(AppModule())
            .build()
    }

    companion object {
        private var appComponent: AppComponent? = null
    }
}
