package dev.art.booksearch.di


import dagger.Module
import dagger.Provides
import dev.art.booksearch.data.network.ApiRestService
import dev.art.booksearch.ui.base.IScheduler
import dev.art.booksearch.ui.base.SchedulerProvider
import javax.inject.Singleton


@Module
class AppModule() {

    @Provides
    @Singleton
    internal fun provideApiRestService(): ApiRestService {
        return ApiRestService.create()
    }

    @Provides
    @Singleton
    internal fun provideScheduler(): IScheduler {
        return SchedulerProvider()
    }
}