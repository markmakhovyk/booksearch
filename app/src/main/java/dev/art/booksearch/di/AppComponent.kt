package dev.art.booksearch.di

import dagger.Component
import dev.art.booksearch.ui.list.ListFragment
import dev.art.booksearch.ui.search.SearchFragment
import javax.inject.Singleton


@Singleton
@Component(modules = [AppModule::class])
interface AppComponent {
    fun inject(searchFragment: SearchFragment)
    fun inject(searchFragment: ListFragment)

}