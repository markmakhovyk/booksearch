package dev.art.booksearch.ui.base

import android.text.Editable
import android.text.TextWatcher

abstract class TextListener : TextWatcher {
    override fun afterTextChanged(p0: Editable?) {
    }

    override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
    }

    override fun onTextChanged(text: CharSequence?, p1: Int, p2: Int, p3: Int) {
        onTextChange(text.toString())
    }

    abstract fun onTextChange(text: String?)
}