package dev.art.booksearch.ui.list

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.NonNull
import androidx.paging.PagedListAdapter
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import dev.art.booksearch.R
import dev.art.booksearch.data.model.Book
import kotlinx.android.synthetic.main.item_list.view.*


internal class ListAdapter constructor(diffUtilCallback: DiffUtil.ItemCallback<Book>) :
    PagedListAdapter<Book, ListAdapter.ListViewHolder>(diffUtilCallback) {

    @NonNull
    override fun onCreateViewHolder(@NonNull parent: ViewGroup, viewType: Int): ListViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_list, parent, false)
        return ListViewHolder(view)
    }

    override fun onBindViewHolder(@NonNull holder: ListViewHolder, position: Int) {
        holder.bind(getItem(position))
    }

    class ListViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bind(book: Book?) {
            itemView.tvTitle.text = book?.title
            itemView.tvAuthor.text = book?.authorName?.joinToString(separator = ",")
            if (book?.imageId != null && book?.imageId != 0) {
                Glide.with(itemView)
                    .load("https://covers.openlibrary.org/w/id/${book?.imageId}.jpg")
                    .into(itemView.ivImage)
            } else {
                itemView.ivImage.setImageResource(R.drawable.ic_book)
            }
        }
    }

}
