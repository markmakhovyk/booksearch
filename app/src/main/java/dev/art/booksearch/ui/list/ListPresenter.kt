package dev.art.booksearch.ui.list


import android.os.Handler
import android.os.Looper
import androidx.paging.PagedList
import dev.art.booksearch.data.model.Book
import dev.art.booksearch.data.repository.BookRepository
import dev.art.booksearch.ui.base.BasePresenter
import dev.art.booksearch.ui.base.IScheduler
import io.reactivex.disposables.Disposable
import java.util.concurrent.Executor
import java.util.concurrent.Executors
import javax.inject.Inject


class ListPresenter @Inject constructor(
    private var repository: BookRepository,
    var provider: IScheduler
) :
    BasePresenter<ListMvpView>() {
    private var dataSource: MyDataSource? = null
    private var disposable: Disposable? = null

    fun getPageList(query: String?): PagedList<Book> {
        dataSource = MyDataSource(repository, query)
        val config = PagedList.Config.Builder()
            .setEnablePlaceholders(false)
            .setPageSize(100)
            .setPrefetchDistance(10)
            .build()

        return PagedList.Builder(dataSource!!, config)
            .setFetchExecutor(Executors.newSingleThreadExecutor())
            .setNotifyExecutor(MainThreadExecutor())
            .build()
    }

    fun initLoadingListener() {
        disposable = dataSource?.getLoading()
            ?.observeOn(provider.mainThread())
            ?.subscribe({
                if (it == MyDataSource.StateLoading.LOADING) {
                    mvpView?.showProgress()
                } else {
                    mvpView?.hideProgress()
                    if (it == MyDataSource.StateLoading.ERROR) {
                        mvpView?.showError(it.runnable)
                    }
                }
            }, {
                mvpView?.hideProgress()
            })
    }

    override fun detachView() {
        dataSource?.clear()
        disposable?.dispose()
        super.detachView()
    }

    inner class MainThreadExecutor : Executor {

        private var mHandler: Handler? = null
        override fun execute(command: Runnable) {
            if (mHandler == null) {
                mHandler = Handler(Looper.getMainLooper())
            }
            mHandler?.post(command)
        }

    }

    fun setDataSource(dataSource: MyDataSource) {
        this.dataSource = dataSource
    }
}
