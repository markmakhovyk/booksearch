package dev.art.booksearch.ui.list

import dev.art.booksearch.ui.base.MvpView

interface ListMvpView : MvpView {
    fun showProgress()
    fun hideProgress()
    fun showError(runnable: Runnable?)

}
