package dev.art.booksearch.ui.search

import android.os.Bundle
import android.view.View
import androidx.core.os.bundleOf
import androidx.navigation.findNavController
import dev.art.booksearch.App
import dev.art.booksearch.R
import dev.art.booksearch.ui.base.AbsFragment
import dev.art.booksearch.ui.base.TextListener
import dev.art.booksearch.ui.list.ListFragment
import dev.art.booksearch.util.hideKeyboard
import kotlinx.android.synthetic.main.fragment_search.*
import javax.inject.Inject


class SearchFragment : AbsFragment(), SearchMvpView {

    override fun showEmpty(query: String) {
        tvMessage.text = getString(R.string.empty_result, query)
        tvMessage.visibility = View.VISIBLE
    }

    override fun showSuccessResult(size: Int, query: String) {
        val bundle = bundleOf(ListFragment.ARG_QUERY to query, ListFragment.ARG_FAUNDED to size)
        view?.findNavController()?.navigate(R.id.action_searchFragment_to_listFragment, bundle)
    }

    override fun showError() {
        tvMessage.text = getString(R.string.default_error)
        tvMessage.visibility = View.VISIBLE
    }

    override fun showProgress() {
        inputSearch.visibility = View.GONE
        progressBar.visibility = View.VISIBLE
        tvMessage.visibility = View.INVISIBLE
    }

    override fun hideProgress() {
        inputSearch.visibility = View.VISIBLE
        progressBar.visibility = View.GONE
    }

    @Inject
    lateinit var presenter: SearchPresenter

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        (activity?.application as App).component?.inject(this)

        presenter.attachView(this)

        btnSearch.setOnClickListener {
            presenter.search(etQuery.text.toString())
            it.hideKeyboard()
        }

        etQuery.addTextChangedListener(object : TextListener() {
            override fun onTextChange(text: String?) {
                btnSearch.isEnabled = !text.isNullOrEmpty()
            }
        })
    }

    override fun provideLayoutResId() = R.layout.fragment_search
}