package dev.art.booksearch.ui.search

import dev.art.booksearch.ui.base.MvpView


interface SearchMvpView : MvpView {
    fun showEmpty(query: String)
    fun showSuccessResult(size: Int, query: String)
    fun showError()
    fun showProgress()
    fun hideProgress()

}
