package dev.art.booksearch.ui.list

import androidx.paging.PositionalDataSource
import dev.art.booksearch.data.model.Book
import dev.art.booksearch.data.repository.BookRepository
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import io.reactivex.subjects.PublishSubject

class MyDataSource(
    private val bookRepository: BookRepository,
    val query: String?
) :
    PositionalDataSource<Book>() {

    var disposable: Disposable? = null
    private val isLoading = PublishSubject.create<StateLoading>()

    override fun loadInitial(
        params: LoadInitialParams,
        callback: LoadInitialCallback<Book>
    ) {
        isLoading.onNext(StateLoading.LOADING)
        disposable = bookRepository.search(query)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .doOnSuccess { isLoading.onNext(StateLoading.SUCCESS) }
            .doOnError {
                val state = StateLoading.ERROR
                state.runnable = Runnable { loadInitial(params, callback) }
                isLoading.onNext(state)
            }
            .subscribe({
                callback.onResult(it.books, 0)
            }, {

            })
    }

    override fun loadRange(
        params: LoadRangeParams,
        callback: LoadRangeCallback<Book>
    ) {
        if (params.startPosition < params.loadSize) {
            callback.onResult(emptyList())
            return
        }
        isLoading.onNext(StateLoading.LOADING)
        disposable = bookRepository.loadPage(query, (params.startPosition / 100) + 1)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .doOnSuccess { isLoading.onNext(StateLoading.SUCCESS) }
            .doOnError {
                val state = StateLoading.ERROR
                state.runnable = Runnable { loadRange(params, callback) }
                isLoading.onNext(state)
            }
            .subscribe({
                callback.onResult(it.books)
            }, {

            })
    }

    fun clear() {
        disposable?.dispose()
    }

    fun getLoading(): Observable<StateLoading> {
        return isLoading
    }

    enum class StateLoading(var runnable: Runnable? = null) {
        LOADING,
        SUCCESS,
        ERROR;
    }
}
