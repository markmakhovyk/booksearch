package dev.art.booksearch.ui.search


import dev.art.booksearch.data.repository.BookRepository
import dev.art.booksearch.ui.base.BasePresenter
import dev.art.booksearch.ui.base.IScheduler
import io.reactivex.disposables.Disposable
import javax.inject.Inject


class SearchPresenter
internal @Inject constructor(private var repository: BookRepository, var provider: IScheduler) :
    BasePresenter<SearchMvpView>() {
    var disposable: Disposable? = null

    fun search(query: String) {
        disposable?.dispose()
        mvpView?.showProgress()
        disposable = repository.search(query)
            .subscribeOn(provider.io())
            .observeOn(provider.mainThread())
            .doOnError { mvpView?.hideProgress() }
            .subscribe(
                { result ->
                    if (result.numFound > 0) {
                        mvpView?.showSuccessResult(result.numFound, query)
                        disposable?.dispose()
                    } else {
                        mvpView?.showEmpty(query)
                    }
                    mvpView?.hideProgress()
                },
                {
                    mvpView?.showError()
                }
            )
    }

}
