package dev.art.booksearch.ui.base


interface Presenter<V : MvpView> {

    fun attachView(mvpView: V)

    fun detachView()

}
