package dev.art.booksearch.ui.base

import io.reactivex.Scheduler
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

interface IScheduler {
    fun io(): Scheduler
    fun mainThread(): Scheduler
}


open class SchedulerProvider : IScheduler {
    override fun io() = Schedulers.io()
    override fun mainThread() = AndroidSchedulers.mainThread()
}

open class SchedulerTestProvider : IScheduler {
    override fun io() = Schedulers.trampoline()
    override fun mainThread() = Schedulers.trampoline()
}
