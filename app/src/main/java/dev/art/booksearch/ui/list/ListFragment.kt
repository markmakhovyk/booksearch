package dev.art.booksearch.ui.list

import android.os.Bundle
import android.view.View
import androidx.recyclerview.widget.LinearLayoutManager
import dev.art.booksearch.App
import dev.art.booksearch.R
import dev.art.booksearch.ui.base.AbsFragment
import kotlinx.android.synthetic.main.fragment_list.*
import javax.inject.Inject


class ListFragment : AbsFragment(), ListMvpView {
    override fun showProgress() {
        layoutError.visibility = View.GONE
        vLoading.visibility = View.VISIBLE
    }

    override fun hideProgress() {
        vLoading.visibility = View.GONE
    }

    override fun showError(runnable: Runnable?) {
        layoutError.visibility = View.VISIBLE
        tvRetry.setOnClickListener {
            runnable?.run()
            it.setOnClickListener(null)
        }
    }

    @Inject
    lateinit var presenter: ListPresenter

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        (activity?.application as App).component?.inject(this)
        presenter.attachView(this)

        val query = arguments?.getString(ARG_QUERY)
        val count = arguments?.getInt(ARG_FAUNDED)
        val adapter = ListAdapter(BookDiffUtilCallback())
        adapter.submitList(presenter.getPageList(query))

        recyclerView.layoutManager = LinearLayoutManager(context)
        recyclerView.adapter = adapter

        tvSearchResult.text = getString(R.string.search_query_result, query, count)

        presenter.initLoadingListener()
    }

    override fun provideLayoutResId() = R.layout.fragment_list

    companion object {
        const val ARG_QUERY = "arg_query"
        const val ARG_FAUNDED = "arg_faunded"
    }
}