package dev.art.booksearch.ui.list

import android.text.TextUtils
import androidx.recyclerview.widget.DiffUtil
import dev.art.booksearch.data.model.Book

class BookDiffUtilCallback : DiffUtil.ItemCallback<Book>() {
    override fun areItemsTheSame(oldItem: Book, newItem: Book): Boolean {
        return oldItem.imageId == newItem.imageId
    }

    override fun areContentsTheSame(oldItem: Book, newItem: Book): Boolean {
        return TextUtils.equals(oldItem.title, newItem.title)
                && TextUtils.equals(
            oldItem.authorName.joinToString(separator = ", "),
            newItem.authorName.joinToString(separator = ", ")
        )
                && oldItem.imageId == newItem.imageId
    }


}