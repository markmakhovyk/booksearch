package dev.art.booksearch.data.network

import dev.art.booksearch.BuildConfig
import dev.art.booksearch.data.model.Response
import io.reactivex.Single
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET
import retrofit2.http.Query
import java.util.concurrent.TimeUnit


interface ApiRestService {

    @GET("search.json")
    fun search(
        @Query("q") query: String?,
        @Query("mode") mode: String? = "ebooks",
        @Query("page") page: Int? = 1,
        @Query("has_fulltext") onlyWithFullText: Boolean? = true
    ): Single<Response>

    companion object Factory {
        fun create(): ApiRestService {
            val retrofit = Retrofit.Builder()
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
                .baseUrl("https://openlibrary.org/")
                .client(provideOkHttpClient())
                .build()

            return retrofit.create(ApiRestService::class.java);
        }

        private fun provideOkHttpClient(): OkHttpClient {
            val okHttpClient = OkHttpClient.Builder()
            okHttpClient.connectTimeout(10000, TimeUnit.MILLISECONDS)
            okHttpClient.readTimeout(10000, TimeUnit.MILLISECONDS)

            val logging = HttpLoggingInterceptor()
            logging.level =
                if (BuildConfig.DEBUG) HttpLoggingInterceptor.Level.BODY else HttpLoggingInterceptor.Level.NONE
            okHttpClient.addInterceptor(logging)

            return okHttpClient.build()
        }
    }
}