package dev.art.booksearch.data.model

import com.google.gson.annotations.SerializedName

data class Book(
    @SerializedName("author_name")
    val authorName: List<String>,
    @SerializedName("cover_i")
    val imageId: Int?,
    val title: String
)