package dev.art.booksearch.data.repository

import dev.art.booksearch.data.model.Response
import dev.art.booksearch.data.network.ApiRestService
import io.reactivex.Single
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class BookRepository @Inject constructor(val apiRestService: ApiRestService) {
    var response: Response? = null
    private fun searchInInternet(query: String?): Single<Response> {
        return apiRestService.search(query)
            .doOnSuccess {
                it.query = query
                response = it
            }
    }

    fun search(query: String?): Single<Response> {
        return if (query == response?.query) {
            Single.just(response)
        } else this.searchInInternet(query)
    }

    fun loadPage(query: String?, page: Int): Single<Response> {
        return apiRestService.search(query = query, page = page)
            .doOnSuccess {
                it.query = query
                response?.books?.plus(it.books)
            }
    }
}