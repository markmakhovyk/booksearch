package dev.art.booksearch.data.model

import com.google.gson.annotations.SerializedName

data class Response(
    @SerializedName("docs")
    val books: List<Book>,
    val numFound: Int,
    val start: Int,
    var query: String?
)